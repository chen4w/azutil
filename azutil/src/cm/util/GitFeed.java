package cm.util;

import java.io.File;
import java.io.FileOutputStream;
import java.net.URL;
import java.util.List;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.CreationHelper;

import com.sun.syndication.feed.synd.SyndEntry;
import com.sun.syndication.feed.synd.SyndFeed;
import com.sun.syndication.io.SyndFeedInput;
import com.sun.syndication.io.XmlReader;


public class GitFeed {
/**
 * 	
 * @param fpath 导出的excel路径
 * @param git_url git.oschina.net 上的issues访问路径,注意项目应设置为开放访问
 * @param page 抓取issues总页数
 * @throws Exception
 */
	public static void toExcel(String fpath, String git_url, int page) throws Exception{
		File resultFile = new File(fpath);
		HSSFWorkbook wb =new HSSFWorkbook(); 
		HSSFSheet sheet = wb.createSheet("Sheet1");  
		HSSFRow row;
		int row_pos=0;
		CellStyle cellStyle = wb.createCellStyle();
		CreationHelper createHelper = wb.getCreationHelper();
		cellStyle.setDataFormat(
		    createHelper.createDataFormat().getFormat("m/d/yy h:mm"));
		
		for(int i=1; i<page+1; i++){
			String rss = git_url;
			if(page>1)
				rss+="&page="+i;
            URL url = new URL(rss);
            // 读取Rss源   
            XmlReader reader = new XmlReader(url);
            
            System.out.println("Rss源的编码格式为：" + reader.getEncoding());
            SyndFeedInput input = new SyndFeedInput();
            // 得到SyndFeed对象，即得到Rss源里的所有信息   
            SyndFeed feed = input.build(reader);
            //System.out.println(feed);
            // 得到Rss新闻中子项列表   
            List entries = feed.getEntries();
            // 循环得到每个子项信息   
			
            for (int j = 0; j < entries.size(); j++) {
    			row = sheet.createRow(row_pos++);
    			int cell_pos=0;

                SyndEntry entry = (SyndEntry) entries.get(j);
                // 标题、连接地址、标题简介、时间是一个Rss源项最基本的组成部分   
				HSSFCell cell = row.createCell(cell_pos++);  
				cell.setCellValue(entry.getTitle());  
                System.out.println("标题：" + entry.getTitle());
                
                cell = row.createCell(cell_pos++);  
				cell.setCellValue(entry.getLink());  
                System.out.println("连接地址：" + entry.getLink());
                
/*                SyndContent description = entry.getDescription();
                cell = row.createCell(cell_pos++);  
				cell.setCellValue(description.getValue());                 
                System.out.println("标题简介：" + description.getValue());
*/                
                
                cell = row.createCell(cell_pos++);  
                cell.setCellStyle(cellStyle);
				cell.setCellValue( entry.getUpdatedDate());                 
                System.out.println("发布时间：" + entry.getUpdatedDate());
                
                
                // 以下是Rss源可先的几个部分   
                cell = row.createCell(cell_pos++);  
				cell.setCellValue( entry.getAuthor());                 
                System.out.println("标题的作者：" + entry.getAuthor());
            }
		}			
			
		FileOutputStream fos = new FileOutputStream(resultFile);  
        // save workbook  
        wb.write(fos);  
        fos.flush();  
        fos.close();  
        wb.close();
	} 
	
	public static void main(String[] args) throws Exception {
		GitFeed.toExcel("/Users/c4w/git/azutil/azutil/resource/cl.xls",
				"https://git.oschina.net/chengxj/cl/issues.atom?assignee_id=&issue_search=&label_name=&milestone_id=&scope=&sort=&status=all",
				21);
	}

}
